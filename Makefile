.PHONY: clean-pyc clean-build docs

help:
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "test - run tests quickly with the default Python"
	@echo "docker-build - build docker image"
	@echo "linter - executes black & mypy on project dir"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +

test:
	pytest tests

linter:
	black --line-length 79 ttl_worker
	mypy ttl_worker --config-file mypy.ini

docker-build:
	docker build --file=./Dockerfile --tag=web_server ./
