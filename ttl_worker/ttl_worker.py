import json
import os
import re
import sys

import logbook
import pika
import redis
from kafka import KafkaProducer
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties

from ttl_worker.consts import (
    RABBIT_HOST_PATTERN,
    REDIS_KEY,
    TOPIC_NAME,
    REDIS_URI_PATTERN,
)


# Setup logging
logbook.StreamHandler(sys.stdout).push_application()
logger = logbook.Logger("web_server")

# Delay in milliseconds for the queue
# DELAY_MS = 60000
DELAY_MS = 5000


class TTLWorker:
    """
    This class contains functionality for the TTLWorker including useful
    connections objects

    Args:
        redis_uri: The redis uri to connect to
        kafka_uri: The kafka uri to connect to
        rabbit_host: The rabbit host to connect to

    Attributes:
        redis_pool: The redis's connection pool
        rabbit_channel: The rabbit's channel to consume
        kafka_producer: The kafka's producer to send messages

    """

    def __init__(self, redis_uri: str, kafka_uri: str, rabbit_host: str):

        self.redis_pool = self.create_redis_pool(redis_uri)
        self.rabbit_channel = self.setup_channels(rabbit_host)
        self.kafka_producer = KafkaProducer(bootstrap_servers=[kafka_uri])

    @staticmethod
    def create_redis_pool(redis_uri: str):
        """
        Creates a Redis connection pool and test the connection to it

        Args:
            redis_uri: The redis's uri

        Raises:
            AttributeError: If the regex pattern is not found in the given URI

        """
        redis_m = re.search(REDIS_URI_PATTERN, redis_uri)
        if redis_m is None:
            raise AttributeError("Redis URI is invalid")
        redis_pool = redis.ConnectionPool(
            host=redis_m.group("host"),
            port=redis_m.group("port"),
            db=redis_m.group("db"),
        )
        conn_test = redis.Redis(connection_pool=redis_pool)
        conn_test.ping()
        logger.debug("Successfully connected to Redis")
        return redis_pool

    @staticmethod
    def setup_channels(rabbit_host: str) -> BlockingChannel:
        """
        Setup the connections for the RabbitMQ, creates the remove queue and
        the remove_delay queue

        Args:
            rabbit_host: The RabbitMQ's host to connect to

        Raises:
            AttributeError: If the regex pattern is not found in the given URI

        Returns: The channel object created

        """
        rabbit_m = re.search(RABBIT_HOST_PATTERN, rabbit_host)
        if rabbit_m is None:
            raise AttributeError("Rabbit host is invalid")
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                rabbit_m.group("host"), int(rabbit_m.group("port"))
            )
        )

        # Create normal tasks channel
        channel = connection.channel()
        channel.confirm_delivery()
        channel.queue_declare(queue="remove", durable=True, auto_delete=False)

        # Bind this channel to an exchange, that will be used to transfer
        # messages from the delay queue
        channel.queue_bind(exchange="amq.direct", queue="remove")

        # Create the delay channel
        delay_channel = connection.channel()
        delay_channel.confirm_delivery()

        # This is where we declare the delay, and routing for our delay channel
        delay_channel.queue_declare(
            queue="remove_delay",
            durable=True,
            arguments={
                "x-message-ttl": DELAY_MS,
                "x-dead-letter-exchange": "amq.direct",
                "x-dead-letter-routing-key": "remove",
            },
        )

        channel.basic_qos(prefetch_count=1)

        return channel

    def on_message(
        self,
        ch: BlockingChannel,
        method: Basic.Deliver,
        props: BasicProperties,
        body: bytes,
    ):
        """
        Method called when receiving a message in the queue
            1. Remove the relevant key from the Redis
            2. Send the value to Kafka

        Note:
            In case there is no value for the key - nothing will be sent to the
            Redis

        Args:
            ch: The channel the message received from
            method: The receiving method
            props: The message's properties
            body: The message contents

        """
        logger.debug(f"Message received: {str(body)}")
        body_data = json.loads(body.decode())
        uid = body_data["uid"]
        r = redis.Redis(connection_pool=self.redis_pool)
        val = r.hget(REDIS_KEY, uid)
        if val is not None:
            r.hdel(REDIS_KEY, uid)
            data = dict(uid=uid, value=val)
            msg = self.kafka_producer.send(
                TOPIC_NAME, json.dumps(data).encode()
            )
            msg.get()
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def run(self):
        """
        Start consuming the Rabbitmq channel for messages
        """
        logger.info("Waiting for messages...")
        self.rabbit_channel.basic_consume(
            queue="remove", on_message_callback=self.on_message
        )
        try:
            self.rabbit_channel.start_consuming()
        except KeyboardInterrupt:
            self.rabbit_channel.stop_consuming()


def main(redis_uri: str, kafka_uri: str, rabbit_host: str):
    """
    Setup preparations and start consuming for calculations

    Args:
        redis_uri: The redis uri to connect to
        kafka_uri: The kafka uri to connect to
        rabbit_host: The rabbit host to connect to

    """
    ttl_worker = TTLWorker(redis_uri, kafka_uri, rabbit_host)
    logger.info("Calc worker started")
    ttl_worker.run()


def init():
    """
    Execute the main and log exceptions
    """
    if __name__ == "__main__":
        try:
            redis_uri = os.environ.get("REDIS", "localhost:6379/0")
            kafka_uri = os.environ.get("KAFKA", "localhost:9092")
            rabbit_host = os.environ.get("RABBIT", "localhost:5672")
            main(redis_uri, kafka_uri, rabbit_host)
        except Exception:
            logger.exception("Calc worker failure")


init()
