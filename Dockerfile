FROM       python
RUN        pip install poetry

WORKDIR    /app
COPY       poetry.lock pyproject.toml /app/

RUN        poetry config virtualenvs.create false --no-interaction --no-ansi
RUN        poetry install --no-root

COPY       . /app
CMD        ["python", "-m", "ttl_worker.ttl_worker"]
