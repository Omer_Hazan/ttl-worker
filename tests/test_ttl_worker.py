import json
from unittest.mock import call

import pika
import pytest
import redis

from ttl_worker import ttl_worker


@pytest.fixture
def ttlworker(mocker):
    redis_mock = mocker.Mock()
    rabbit_mock = mocker.Mock()
    kafka_mock = mocker.Mock()
    mocker.patch.object(ttl_worker.TTLWorker, "create_redis_pool",
                        return_value=redis_mock)
    mocker.patch.object(ttl_worker.TTLWorker, "setup_channels",
                        return_value=rabbit_mock)
    mocker.patch.object(ttl_worker, "KafkaProducer", return_value=kafka_mock)
    worker = ttl_worker.TTLWorker("localhost:1234/0", "localhost:123",
                                  "localhost:121")
    return worker, redis_mock, rabbit_mock, kafka_mock


def test_on_message(mocker, ttlworker):
    """
    Test on_message happy flow
    """
    uid, value = 1, 1
    worker, _, _, kafka_mock = ttlworker
    patcher = mocker.patch('redis.Redis')
    redis_mock = patcher.return_value
    redis_mock.hget.return_value = value
    body = json.dumps(dict(uid=uid)).encode()
    worker.on_message(mocker.Mock(), mocker.Mock(), mocker.Mock(), body)
    expected_body = json.dumps(dict(uid=uid, value=value)).encode()
    kafka_mock.send.assert_called_with(ttl_worker.TOPIC_NAME, expected_body)


def test_setup_channels(mocker):
    """
    Test happy flow functionality for setup_channels
    """
    queue_declare_mock = mocker.Mock()
    channel_mock = mocker.Mock()
    queue_bind_mock = mocker.Mock()

    channel_mock.queue_declare = queue_declare_mock
    channel_mock.queue_bind = queue_bind_mock

    patcher = mocker.patch.object(pika, 'BlockingConnection')
    mocker.patch.object(pika, 'ConnectionParameters')

    redis_mock = patcher.return_value
    redis_mock.channel.return_value = channel_mock

    ttl_worker.TTLWorker.setup_channels('localhost:8000')

    queue_bind_mock.assert_called_with(exchange="amq.direct", queue="remove")
    declare_calls = [call(queue="remove", durable=True, auto_delete=False),
                     call(queue="remove_delay",
                          durable=True,
                          arguments={
                              "x-message-ttl": ttl_worker.DELAY_MS,
                              "x-dead-letter-exchange": "amq.direct",
                              "x-dead-letter-routing-key": "remove",
                          })]
    queue_declare_mock.assert_has_calls(declare_calls)


@pytest.mark.parametrize("pattern, exception", [('', AttributeError),
                                                ('localhost', AttributeError),
                                                ('1234', AttributeError)])
def test_redis_invalid_uri_pattern(pattern, exception):
    """
    Tests the case when giving setup_redis invalid argument
    """
    with pytest.raises(exception):
        ttl_worker.TTLWorker.create_redis_pool(pattern)


@pytest.mark.parametrize("pattern, exception", [('', AttributeError),
                                                ('localhost', ValueError),
                                                ('1234', ValueError)])
def test_rabbit_invalid_uri_pattern(pattern, exception):
    """
    Tests the case when giving setup_rabbitmq invalid argument
    """
    with pytest.raises(exception):
        ttl_worker.TTLWorker.setup_channels(pattern)


def test_create_redis_pool_happy_flow(mocker):
    """
    Test create_redis_pool happy flow functionality
    """
    mocker.patch.object(redis, 'ConnectionPool')
    patcher = mocker.patch.object(redis, 'Redis')
    redis_mock = patcher.return_value
    ping_mock = mocker.Mock()
    redis_mock.ping = ping_mock
    ttl_worker.TTLWorker.create_redis_pool("localhost:8000/0")
    ping_mock.assert_called_once()


def test_main_happy_flow(ttlworker):
    """
    Test main happy flow functionality
    """
    worker, _, rabbit_mock, _ = ttlworker
    ttl_worker.main("a", "b", "c")
    rabbit_mock.start_consuming.assert_called_once()


def test_main_keyboardinterrupt(ttlworker):
    """
    Test main when KeyboardInterrupt raises
    """
    worker, _, rabbit_mock, _ = ttlworker
    rabbit_mock.start_consuming.side_effect = KeyboardInterrupt()
    ttl_worker.main("a", "b", "c")
    rabbit_mock.stop_consuming.assert_called_once()


def test_init(mocker):
    """
    Test module init (__main__)
    """
    mocker.patch.object(ttl_worker, "__name__", "__main__")
    main_mock = mocker.patch.object(ttl_worker, "main")
    ttl_worker.init()
    main_mock.assert_called_once()


def test_init_exception(mocker):
    """
    Test logging when exception occurs in main
    """
    mocker.patch.object(ttl_worker, "__name__", "__main__")
    mocker.patch.object(ttl_worker, "main", side_effect=ValueError)

    logger_mock = mocker.patch.object(ttl_worker.logger, "exception")
    ttl_worker.init()
    logger_mock.assert_called_once()
